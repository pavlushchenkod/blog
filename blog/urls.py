from django.contrib import admin
from django.conf.urls import include, url
from django.urls import path
from blogapp import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.home_page, name='home'),
    url(r'^blogapp/', include('blogapp.urls')),
    url(r'^admin/', admin.site.urls),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

