from django import forms


class CommentForm(forms.Form):
    comment = forms.CharField(label='comment', max_length=300)


class RegistrationForm(forms.Form):
    name = forms.CharField(label='name', max_length=10)
    email = forms.EmailField(label='email')
    password = forms.CharField(label='password', max_length=20)