from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse
from django.db.models import F
from django.core.paginator import Paginator, PageNotAnInteger
from django.db import IntegrityError
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, AnonymousUser

from blogapp.models import Article, Categories, Comments
from .forms import CommentForm


def home_page(request):
    articles_list = Article.objects.order_by('-pub_date')
    categories = Categories.objects.all()

    paginator = Paginator(articles_list, 3)
    page = request.GET.get('page')

    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)

    context = {'articles': articles, 'categories': categories}
    return render(request, 'blogapp/main.html', context)


def show_article(request, article_id):
    if request.method == 'GET':
        comments = Comments.objects.filter(article_id=article_id)
        article = Article.objects.get(id=article_id)
        context = {'article': article, 'article_id': article_id, 'comments': comments}
        return render(request, 'blogapp/article.html', context)

    if request.method == 'POST':

        if isinstance(request.user, AnonymousUser):
            return redirect('user_login')

        if request.user:
            if not request.user.is_authenticated:
                return redirect('user_login')
            form = CommentForm(request.POST)

            if form.is_valid():
                user_id = request.user.id

                comment = Comments(
                    text=form.cleaned_data['comment'],
                    article_id=article_id,
                    user_id=user_id)
                comment.save()

                Article.objects.filter(id=article_id).update(quantity_comments=F('quantity_comments') + 1)
            return HttpResponseRedirect(reverse('show_article', args=(str(article_id))))

        else:
            return redirect('user_login')


def articles_in_category(request, category_name):
    articles_list = Article.objects.filter(categories__name=category_name).order_by('-pub_date')
    categories = Categories.objects.all()

    paginator = Paginator(articles_list, 3)
    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
    except PageNotAnInteger:
        articles = paginator.page(1)
    context = {'articles': articles, 'categories': categories}
    return render(request, 'blogapp/main.html', context)


def registration(request):
    if request.method == 'GET':
        return render(request, 'blogapp/registration.html')
    if request.method == 'POST':

        if 'email' in request.session:
            return redirect('home_page')
        data = request.POST
        error = None

        if 15 < len(data['name']) < 3:
            error = 'Incorrect Name!'
        elif '@' not in data['email']:
            error = 'Incorrect Email!'
        elif len(data['password']) < 6 or len(data['password']) > 20:
            error = 'Incorrect Password!'

        if error is not None:
            context = {'Error':error}
            return render(request, 'blogapp/registration.html', context)

        user = User.objects.create_user(
            username=data['name'],
            email=data['email'],
            password=data['password'])
        try:
            user.save()
        except IntegrityError:
            error = 'Email already used!'
            context = {'Error': error}
            return render(request, 'blogapp/registration.html', context)
        request.session['email'] = data['email']
        return redirect('home_page')


def user_login(request):
    if request.method == 'GET':
        return render(request, 'blogapp/user_login.html')

    if request.method == 'POST':
        if not request.user.is_authenticated:
            data = request.POST
            user = authenticate(username=data['name'],
                                password=data['password'])
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('home_page')
        return redirect('home_page')


def user_logout(request):
    logout(request)
    return redirect('home_page')
