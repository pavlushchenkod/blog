from django.contrib import admin

from .models import Users, Article, Categories, Comments

admin.site.register(Users)
admin.site.register(Article)
admin.site.register(Categories)
admin.site.register(Comments)