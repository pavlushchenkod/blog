from django.db import models
from django.contrib.auth.models import User


class Users(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=20, default=None)

    def __str__(self):
        return self.name


class Categories(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField(upload_to='categories', default='categories/default_categories.jpg')

    def __str__(self):
        return self.name


class Article(models.Model):
    name = models.CharField(max_length=100)
    categories = models.ManyToManyField(Categories, db_table='ManyToMany_Articles_vs_Categories')
    description = models.CharField(max_length=200)
    body_text = models.TextField()
    pub_date = models.DateField(auto_now_add=True)
    edit_date = models.DateField(auto_now=True)
    quantity_comments = models.IntegerField(default=0)
    quantity_likes = models.IntegerField(default=0)
    image = models.ImageField(upload_to='articles', default='articles/default_article.jpg')

    def __str__(self):
        return self.name


class Comments(models.Model):
    text = models.TextField()
    pub_datetime = models.DateTimeField(auto_now_add=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.text

