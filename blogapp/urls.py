from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [

    url(r'^$', views.home_page, name='home_page'),
    url(r'^(?P<article_id>[0-99]+)/$', views.show_article, name='show_article'),
    url(r'^articles_in_category/(?P<category_name>[A-Za-z0-9-]+)/$', views.articles_in_category, name='articles_in_category'),
    url(r'^registration$', views.registration, name='registration'),
    url(r'^login$', views.user_login, name='user_login'),
    url(r'^logout$', views.user_logout, name='user_logout'),

]

