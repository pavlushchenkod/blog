# Generated by Django 2.0 on 2018-01-13 12:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blogapp', '0006_auto_20171227_1426'),
    ]

    operations = [
        migrations.AddField(
            model_name='users',
            name='password',
            field=models.CharField(default=None, max_length=20),
        ),
    ]
